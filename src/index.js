import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import App from'./App.js';

class Component extends React.Component {
    
    render(){
        return(
            <React.Fragment>
                <App/>
            </React.Fragment>
        );
    }
}


ReactDOM.render(<Component/> ,document.getElementById('root'));

serviceWorker.unregister();
