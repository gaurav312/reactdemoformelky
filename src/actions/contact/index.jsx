// SignUpCreator
import axios from "axios"
import { SIGNUP_API, USER_OTP_API, RESEND_OTP } from "../../actionTypes/index.jsx"
import { ApiBaseUrl } from "../../utils/BaseUrls.jsx";

export const SignupApi = (data) => {
  return (dispatch) => {
    axios.post(ApiBaseUrl + '/api/register/', data, { headers: {'Accept': 'application/json'}})
     .then(function (response) {
       dispatch({
         type:SIGNUP_API,
         payload: { status: 200, data: response.data }
       })
    })
    .catch(function (error) {
       dispatch({
         type:SIGNUP_API,
         payload: { status: 400, error: error.response.data }
       })
    })
  }
}
