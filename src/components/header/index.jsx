import React from'react';
import {NavLink, HashRouter} from "react-router-dom";
class Header extends React.Component{
    render(){
        return(
            <React.Fragment>
                <HashRouter>
                    <header className="main_header f_14_16">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-4 text-white">
                                    <a rel="noopener noreferrer" href="https://www.facebook.com/hotelcrownpalace/" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Facebook" >
                                        <i className="fab fa-facebook-f"></i>
                                    </a>
                                    <a rel="noopener noreferrer" href="https://twitter.com/infocrownpalace" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Twitter" >
                                        <i className="fab fa-twitter"></i>
                                    </a>
                                    {/* <a rel="noopener noreferrer" href="https://twitter.com/infocrownpalace" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Google Plus Plus" >
                                        <i className="fab fa-google-plus-g"></i>
                                    </a> */}
                                    <a rel="noopener noreferrer" href="https://in.pinterest.com/infocrownpalace/" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Pinterest" >
                                        <i className="fab fa-pinterest-p"></i>
                                    </a>
                                    <a rel="noopener noreferrer" href="https://www.youtube.com/watch?v=5GATB4N8TEM" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Youtube" >
                                        <i className="fab fa-youtube"></i>
                                    </a>
                                    <a rel="noopener noreferrer" href="https://www.instagram.com/p/BlLPDlTFI4C/" target="_blank" className="w_30 d-inline-block text_primary text-center p_y_10" title="Instagram" >
                                        <i className="fab fa-instagram"></i>
                                    </a>
                                </div>
                                <div className="col-12 col-sm-4 text-center">
                                 <NavLink to="/" className="d-inline-block site_logo"><img src="./assets/images/logo.png" alt="Logo"/></NavLink>
                                </div>
                                <div className="col-12 col-sm-4 text-white text-right">
                                 <span className="p_y_10 d-inline-block m_r_10"><i className="fas fa-phone text_primary m_r_5"></i> +91 07312528855</span>
                                    <span className="p_y_10  d-inline-block"><i className="fas fa-map-marker-alt text_primary m_r_5"></i>2-A, Near Geeta Bhavan Square, Kanchan Bagh, Indore, Madhya Pradesh - 452001</span>
                                </div>
                            </div>  
                        </div> 
                        <div className="bottom_header bg_light clearfix bg-white">
                            <div className="container">
                                <div className="navigations">
                                    <nav className="navbar navbar-expand-lg navs navbar-light">
                                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></button>
                                    <div className="collapse navbar-collapse" id="navbarNav">
                                        <ul className="navbar-nav mx-auto">
                                            <li className="nav-item"><NavLink to="/" className="nav-link ">Home</NavLink></li>
                                            <li className="nav-item"><NavLink to="/about" className="nav-link" >About</NavLink></li>
                                            <li className="nav-item"><NavLink to="/room" className="nav-link ">Rooms</NavLink></li>
                                            <li className="nav-item"><NavLink to="/restaurant" className="nav-link ">Restaurants</NavLink></li>
                                            <li className="nav-item"><NavLink to="/banquet" className="nav-link ">Banquet</NavLink></li>
                                            <li className="nav-item"><NavLink to="/contact" className="nav-link ">Contact Us</NavLink></li>
                                        </ul>
                                    </div>
                                    </nav>
                                
                                </div>
                            </div>
                        </div>
                    </header>
                </HashRouter> 
            </React.Fragment>
        );
    }
}
export default Header;