import React, { Component, Fragment} from'react';
import Header from '../header'
import Footer from '../footer'
 
class About extends Component { 

    render(){

        return(
            <Fragment>
            <Header />
            <section className="bg-white p_b_50">
                {/* <h1 className="ptb_50_40 bg_primary  overflow-hidden m-0 w-100 f_30_34 font-weight-bold text-white text-center">About Us</h1> */}
                <div className="mx_auto"> <img src="./assets/images/about.jpg"  alt="main" className="m_b_20 w-100" /></div>
                <div className="card w_800 mx_auto border-0 r_10 box_shw2 m_t_n100">
                    <div className="card-body p_40">
                        <h1 className="text_primary m_b_40 f_30_34 font-weight-bold text-center">About</h1>
                         <p>Located in the heart of Indore city, Hotel Crown Palace holds a revered place in the hearts of the people of Indore. Established in the year 1990, we have been an integral part of the lives of the people of this city for decades. We welcome you into our midst of elegance, sophistication and true and selfless hospitality.<br/><br/>
                        Being a 3 star accredited Hotel by Indian Tourism (Govt. of India); we aspire to provide you the best of all services here. With 25 successful years in the hospitality industry we offer you a warm stay with us and look forward to cater to your special needs.</p>
                    </div>
                </div>
            </section>
            <Footer /   >
            </Fragment>
        );
    }
}

export default About;