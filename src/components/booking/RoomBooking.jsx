import React, { Component, Fragment} from'react';
import Header from '../header'
import Footer from '../footer'
import qs from "query-string";

class RoomBooking extends Component {

    constructor() {
        let url = qs.parse(window.location.search);
        super();
        this.state = {
            checkin: url.checkin,
            checkout: url.checkout,
            rooms: [
                { room: "1", adult: "1", kid: "5" }
            ],
        };
    }
    
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }
    
    render() {

        return(
            <Fragment>
                <Header />
                <section className="bg-white p_y_50">
                    <div className="card w_800 mx_auto border-0 r_10 box_shw2">
                        <div className="card-body p_y_50 p_x_40 reservation-form position-static bottom_0">
                            <h1 className="text_primary m_b_40 f_30_34 font-weight-bold text-center">Booking Form</h1>
                            <form className="booking-form" method="get"  autoComplete="off" onSubmit={this.handleSubmit}>
                                <div className="form-row">
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="checkindate" className="text_primary">Check In Date <span className="text-danger">*</span> 
                                            </label>
                                             <input 
                                                type="text" 
                                                className="form-control rounded-0" 
                                                name="checkin" 
                                                id="checkindate" 
                                                placeholder="DD/MM/YYYY" 
                                                size="30" 
                                                required="required"
                                                value={this.state.checkin}
                                            />
                                            <label htmlFor="checkindate"> 
                                                <span className="date-icon p-1">
                                                    <i className="far fa-calendar-alt"></i>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="checkoutdate" className=" text_primary">Check out Date <span className="text-danger">*</span>
                                            </label>
                                            <input 
                                                type="text" 
                                                className="form-control rounded-0" 
                                                name="checkout" 
                                                id="checkoutdate" 
                                                placeholder="DD/MM/YYYY" 
                                                size="30" 
                                                required="required"
                                                value={this.state.checkout}

                                            />
                                            <label htmlFor="checkoutdate"> <span className="date-icon p-1"><i className="far fa-calendar-alt"></i></span>
                                            </label>
                                        </div>
                                    </div>
                                        
                                    <div className="shareholder">
                                        <div className="row">
                                            <div className="col-12 col-md-4">
                                                <div className="form-group mb-0">
                                                    <label htmlFor="number_of_adults" className=" text_primary">Room <span className="text-danger">*</span>
                                                    </label>
                                                    <input 
                                                        type="number" 
                                                        className="form-control m_b_30 rounded-0" 
                                                        name="room" id="number_of_adults" 
                                                        size="30" 
                                                        required="required" 
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-4">
                                                <div className="form-group mb-0">
                                                    <label htmlFor="number_of_adults" className=" text_primary">Adults <span className="text-danger">*</span>
                                                    </label>
                                                    <input 
                                                        type="number" 
                                                        className="form-control m_b_30 rounded-0" 
                                                        name="adult" 
                                                        id="number_of_adults" 
                                                        placeholder="0" 
                                                        size="30" 
                                                        required="required"
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-4">
                                                <div className="form-group mb-0">
                                                    <label htmlFor="number_of_adults" className=" text_primary">Kids (2-8 years) <span className="text-danger">*</span>
                                                    </label>
                                                    <input 
                                                        type="number" 
                                                        className="form-control m_b_30 rounded-0" 
                                                        name="kid" 
                                                        id="number_of_adults" 
                                                        placeholder="0" 
                                                        size="30" 
                                                        required="required" 
                                                    />
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div className="form-group col-12 mb-0 text-center mt-2">
                                        <button onClick={this.continue} className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white" >Continue</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <Footer />
            </Fragment>
        );
    }
}

export default RoomBooking;