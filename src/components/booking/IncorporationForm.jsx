import React, { Component, Fragment} from'react';

class IncorporationForm extends Component {

        constructor() {
          super();
          this.state = {
            name: "",
            shareholders: [{ name: "" }]
          };
        }
      
        handleNameChange = evt => {
          this.setState({ name: evt.target.value });
        };
      
        handleShareholderNameChange = idx => evt => {
          const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
            if (idx !== sidx) return shareholder;
            return { ...shareholder, name: evt.target.value };
          });
      
          this.setState({ shareholders: newShareholders });
        };
      
   
      
        handleAddShareholder = () => {
          this.setState({
            shareholders: this.state.shareholders.concat([{ name: "" }])
          });
        };
      
        handleRemoveShareholder = idx => () => {
          this.setState({
            shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx)
          });
        };


        handleSubmit = evt => {
            evt.preventDefault();
            console.log('state',this.state);
            // const { name, shareholders } = this.state;
            // alert(`Incorporated: ${name} with ${shareholders.length} shareholders`);
          };
    render() {
        return(
            <Fragment>
                <form onSubmit={this.handleSubmit}>
                    {this.state.shareholders.map((shareholder, idx) => (
                        <div className="shareholder" key={idx}>
                            <input
                            type="text"
                            placeholder={`Shareholder #${idx + 1} name`}
                            value={idx + 1}
                            onChange={this.handleShareholderNameChange(idx)}
                            />
                            <button
                            type="button"
                            onClick={this.handleRemoveShareholder(idx)}
                            className="small"
                            >
                            -
                            </button>
                        </div>
                    ))}
                <button
                type="button"
                onClick={this.handleAddShareholder}
                className="small"
                >
                Add Shareholder
                </button>
                <button>Incorporate</button>
            </form>
            </Fragment>
        );
    }
}

export default IncorporationForm;

// ------------------------------------------
// import React, { Component, Fragment} from'react';
// import Header from '../header'
// import Footer from '../footer'
// import qs from "query-string";

// class RoomBooking extends Component {

//     constructor() {
//         let url = qs.parse(window.location.search);
//         super();
//         this.state = {
//             checkin: url.checkin,
//             checkout: url.checkout,
//             rooms: [
//                 { room: "1", adult: "1", kid: "5" }
//             ],
//         };
//     }

//     handleShareholderNameChange = idx => evt => {
//         const newrooms = this.state.rooms.map((shareholder, sidx) => {
//         if (idx !== sidx) return shareholder;
//             let name = evt.target.name
//             return { ...shareholder, [name]: evt.target.value };
//         });
//         this.setState({ rooms: newrooms });
//     };

//     handleAddShareholder = () => {
//         let room_len = this.state.rooms.length + 1
//         let new_room = room_len
//         this.setState({
//             rooms: this.state.rooms.concat([{ room: new_room.toString(), adult: "1", kid: "0" }])
//         });
//     };

//     handleRemoveShareholder = idx => () => {
//         console.log(idx)
//         this.setState({
//             rooms: this.state.rooms.filter((s, sidx) => idx !== sidx)
//         });
//         //TODO Reindexing filter  
//     };
    

//     /* Form submition */
//     handleSubmit = evt => {
//         if(this.handleValidation()) {
//             this.setState({ 
//                 formSubmition: true                
//             });
//             evt.preventDefault();
//             console.log(this.state);
//         } else {
//             evt.preventDefault();
//             alert('Form not submit');
//         }
//     };
    
//     render() {
//         return(
//             <Fragment>
//                 <Header />
//                 <section className="bg-white p_y_50">
//                     <div className="card w_800 mx_auto border-0 r_10 box_shw2">
//                         <div className="card-body p_y_50 p_x_40 reservation-form position-static bottom_0">
//                             <h1 className="text_primary m_b_40 f_30_34 font-weight-bold text-center">Booking Form</h1>
//                             <form className="booking-form" method="get" action="#/details" autoComplete="off" onSubmit={this.handleSubmit}>
//                                 <div className="form-row">
//                                     <div className="col-12 col-md-6">
//                                         <div className="form-group mb-0">
//                                             <label htmlFor="checkindate" className="text_primary">Check In Date <span className="text-danger">*</span> 
//                                             </label>
//                                              <input 
//                                                 type="text" 
//                                                 className="form-control rounded-0" 
//                                                 name="checkin" 
//                                                 id="checkindate" 
//                                                 placeholder="DD/MM/YYYY" 
//                                                 size="30" 
//                                                 required="required"
//                                                 value={this.state.checkin}
//                                                 onChange={this.handleShareholderNameChange}
//                                             />
//                                             <label htmlFor="checkindate"> 
//                                                 <span className="date-icon p-1">
//                                                     <i className="far fa-calendar-alt"></i>
//                                                 </span>
//                                             </label>
//                                         </div>
//                                     </div>
//                                     <div className="col-12 col-md-6">
//                                         <div className="form-group mb-0">
//                                             <label htmlFor="checkoutdate" className=" text_primary">Check out Date <span className="text-danger">*</span>
//                                             </label>
//                                             <input 
//                                                 type="text" 
//                                                 className="form-control rounded-0" 
//                                                 name="checkout" 
//                                                 id="checkoutdate" 
//                                                 placeholder="DD/MM/YYYY" 
//                                                 size="30" 
//                                                 required="required"
//                                                 value={this.state.checkout}
//                                                 onChange={this.handleShareholderNameChange}
//                                             />
//                                             <label htmlFor="checkoutdate"> <span className="date-icon p-1"><i className="far fa-calendar-alt"></i></span>
//                                             </label>
//                                         </div>
//                                     </div>
                                
                                
//                                     {this.state.rooms.map((room, idx) => (
                                        
//                                         <div className="shareholder" key={idx}>
//                                             <div className="row">
//                                                 <div className="col-12 col-md-3">
//                                                     <div className="form-group mb-0">
//                                                         <label htmlFor="number_of_adults" className=" text_primary">Room <span className="text-danger">*</span>
//                                                         </label>
//                                                         <input 
//                                                             type="number" 
//                                                             className="form-control m_b_30 rounded-0" 
//                                                             name="room" id="number_of_adults" 
//                                                             size="30" 
//                                                             required="required" 
//                                                             value={ room.room } 
//                                                             onChange={this.handleShareholderNameChange(idx)}
//                                                             />
//                                                     </div>
//                                                 </div>
//                                                 <div className="col-12 col-md-3">
//                                                     <div className="form-group mb-0">
//                                                         <label htmlFor="number_of_adults" className=" text_primary">Adults <span className="text-danger">*</span>
//                                                         </label>
//                                                         <input 
//                                                             type="number" 
//                                                             className="form-control m_b_30 rounded-0" 
//                                                             name="adult" 
//                                                             id="number_of_adults" 
//                                                             placeholder="0" 
//                                                             size="30" 
//                                                             required="required" 
//                                                             value={ room.adult }
//                                                             onChange={this.handleShareholderNameChange(idx)} 
//                                                         />
//                                                     </div>
//                                                 </div>
//                                                 <div className="col-12 col-md-3">
//                                                     <div className="form-group mb-0">
//                                                         <label htmlFor="number_of_adults" className=" text_primary">Kids (2-8 years) <span className="text-danger">*</span>
//                                                         </label>
//                                                         <input 
//                                                             type="number" 
//                                                             className="form-control m_b_30 rounded-0" 
//                                                             name="kid" 
//                                                             id="number_of_adults" 
//                                                             placeholder="0" 
//                                                             size="30" 
//                                                             required="required" 
//                                                             value={ room.kid }
//                                                             onChange={this.handleShareholderNameChange(idx)}
//                                                         />
//                                                     </div>
//                                                 </div>
//                                                 <div className="col-12 col-md-3">
//                                                     <div className="form-group mb-0">
//                                                         <label htmlFor="number_of_adults" className=" text_primary"> <span className="text-danger"></span>
//                                                         </label><br/>
//                                                         <button
//                                                             type="button"
//                                                             className="rounded-0 small mt-2 btn-danger"
//                                                             onClick={this.handleRemoveShareholder(idx)}
//                                                         >
//                                                         Remove -
//                                                         </button>
//                                                     </div>
//                                                 </div>
                                            
//                                             </div>
//                                         </div>
//                                     ))}
//                                     <div className="col-12 m_t_n20 m_b_20 c_p">
//                                         <span className="text_primary f_12_14" onClick={this.handleAddShareholder}>Add More Room <i className="fas fa-plus"></i></span>
//                                         <button className="Next" onClick={this.continue}>Next>></button>
//                                     </div>
//                                     <div className="form-group col-12 mb-0 text-center mt-2">
//                                         <input type="submit" value="Continue" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white" />
//                                     </div>
//                                 </div>
//                             </form>
//                         </div>
//                     </div>
//                 </section>
//                 <Footer />
//             </Fragment>
//         );
//     }
// }

// export default RoomBooking;