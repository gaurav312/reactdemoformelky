import React, { Component, Fragment} from'react';
import Header from '../header'
import Footer from '../footer'

class userDetails extends Component {

    constructor() {
        super();
        this.state = {
            username: "",
            email: "",
            phone: "",
            address: "",
        };
        this.detailsFormsubmit = this.detailsFormsubmit.bind(this);
        this.handleUserDetails = this.handleUserDetails.bind(this);

    }

    
    
    /*Onchange user details */
    handleUserDetails (event) {
        let name = event.target.name;
        this.setState({[name]: event.target.value});          
    };

    /*User details form */
    detailsFormsubmit() {
        console.log(this.state);

    }

    render() {
     
        return(
            <Fragment>
                <Header />
                <section className="bg-white p_y_50">
                    <div className="card w_800 mx_auto border-0 r_10 box_shw2">
                        <div className="card-body p_y_50 p_x_40 reservation-form position-static bottom_0">
                            <h1 className="text_primary m_b_40 f_30_34 font-weight-bold text-center">Details</h1>
                            <form className="booking-form" method="get" action="#/payment" autoComplete="off" onSubmit={this.detailsFormsubmit}>
                                <div className="form-row">
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="username" className="text_primary">Name<span className="text-danger">*</span> 
                                            </label>
                                            <input 
                                                type="text" 
                                                className="form-control rounded-0" 
                                                name="username" 
                                                id="username" 
                                                placeholder="Name" 
                                                size="30" 
                                                required="required" 
                                                value={this.state.username}
                                                onChange={this.handleUserDetails}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="email" className=" text_primary">Email Address<span className="text-danger">*</span>
                                            </label>
                                            <input 
                                                type="email" 
                                                className="form-control rounded-0" 
                                                name="email" 
                                                id="email" 
                                                placeholder="email" 
                                                size="30" 
                                                required="required"
                                                value={this.state.email}
                                                onChange={this.handleUserDetails}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="phone" className=" text_primary">Phone Number<span className="text-danger">*</span>
                                            </label>
                                            <input 
                                                type="text" 
                                                className="form-control rounded-0" 
                                                name="phone" 
                                                id="phone" 
                                                placeholder="phone" 
                                                size="30" 
                                                required="required"
                                                value={this.state.phone}
                                                onChange={this.handleUserDetails}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-6">
                                        <div className="form-group mb-0">
                                            <label htmlFor="address" className=" text_primary">Address <span className="text-danger">*</span>
                                            </label>
                                            <input 
                                                type="text" 
                                                className="form-control rounded-0" 
                                                name="address" 
                                                id="address" 
                                                placeholder="address" 
                                                size="30" 
                                                required="required"
                                                value={this.state.address}
                                                onChange={this.handleUserDetails}
                                            />
                                        </div>
                                    </div>
                                    
                                    <div className="form-group col-12 mb-0 text-center mt-2">
                                        <input type="submit" value="Continue" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <Footer />
            </Fragment>
        );   
    }
}

export default userDetails;