import React, { Component, Fragment} from'react';
import Header from '../header';
import Footer from '../footer';

class Contact extends Component{

    constructor(props) {
        super(props);
        this.initianalState = {
            name: "",
            email: "",
            phone: "",
            message: ""
        };
        this.state = this.initianalState;
    }
          
    handleChange(event) {
        let name = event.target.name;
        this.setState({[name]: event.target.value});
    }

    contactFrom(event) {
        event.preventDefault();
        let data = {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            message: this.state.message,
        }
        console.log(data);
        // this.props.ContactFormApi(data);
    }

    render(){
        return(
        <Fragment>
        <Header />
        <section className="bg-white p_b_50">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7360.446524050914!2d75.8827291!3d22.7199416!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3962fd3be6a104db%3A0xa027d49b6aa49c2e!2sHotel+Crown+Palace!5e0!3m2!1sen!2sin!4v1553760068431" height="450" title ="googlemap" frameborder="0" allowfullscreen="" className="w-100"></iframe>
            <div className="contact_page_card border-0 card bx_shw2 move_up_100 mx-auto pdng_40 pdng_l_30 pdng_r_30 mrg_b_40 w-100">
                <div className="row">
                    <div className="col-md-6">
                        <h3 className="contact_heading mrg_b_20 text_primary">Write to Us:</h3>
                        <form className="contact_form">
                            <div className="form_in form-group pb-4">
                                <label className="text-black d-none">Name<span className="text-danger">*</span></label>
                                <span className="c_spans text-danger float-right">*</span>
                                <input type="text" className="form-control pl-0" id="name" placeholder="Name" autocomplete="off" required="required"/>
                            </div>

                                        <div className="form_in form-group pb-4">
                                            <label className="text-black d-none">Email<span className="text-danger">*</span></label>
                                            <span className="c_spans text-danger float-right">*</span>
                                        </div>

                                        <div className="form_in form-group">
                                            <label className="text-black d-none">Number<span className="text-danger">*</span></label>
                                            <span className="c_spans text-danger float-right">*</span>
                                            <input type="email" name="email" value={this.state.email} onChange={this.handleChange.bind(this)} className="form-control pl-0" id="username" placeholder="Email" autoComplete="off" required="required" />
                                            <input type="text"  name="phone" value={this.state.phone} onChange={this.handleChange.bind(this)} className="form-control pl-0" id="number" placeholder="Contact Number" autoComplete="off" required="required" href="http://test.com" />
                                        </div>

                                        <div className="form_in form-group">
                                            <label className="text-black d-none">Message<span className="text-danger">*</span></label>
                                            <span className="c_spans text-danger float-right">*</span>
                                            <textarea placeholder="Message" name="message" value={this.state.message} onChange={this.handleChange.bind(this)} className="form-control pl-0" rows="5" id="message" required="required" href="http://test.com"></textarea>
                                        </div>

                                        <div className="btn_wr">
                                            <button type="submit" className="btn text-uppercase text-white float-right agent_btn_cancel">submit</button>
                                        </div>
                                    </form> 
                                </div>
            
                                <div className="col-md-6">
                                    <h3 className="contact_heading mrg_b_20 text_primary">Contacts:</h3>
                                    <ul className="list-group list-group-flush contact_listing">
                                        <li className="list-group-item px-0 border-top-0">Rooms Reservation: <span className="float-right lis_res">+91-07312528855</span></li>
                                        <li className="list-group-item px-0">Banquets: <span className="float-right lis_res">+91-8109010159</span></li>
                                        <li className="list-group-item px-0">Bakewell Bakery: <span className="float-right lis_res">+91-07312528877</span></li>
                                        <li className="list-group-item px-0">Take Away Parcel: <span className="float-right lis_res">+91-07312528875</span></li>
                                        <li className="list-group-item px-0">Xpress Bakewell Geeta Bhawan: <span className="float-right lis_res">+91-7773002901</span></li>
                                        <li className="list-group-item px-0">Xpress Bakewell Anand Bazar: <span className="float-right lis_res">+91-7773002902</span></li>
                                    </ul>
                                    <div className="pdng_t_30">   
                                        <h3 className="contact_heading mrg_b_20 text_primary">Address:</h3>
                                        <p>2-A, Near Geeta Bhavan Square, Kanchan Bagh, Indore, Madhya Pradesh - 452001</p> 
                                        <ul className="list-group list-group-flush contact_listing">
                                            <li className="list-group-item px-0 border-top-0">Distance from Airport: <span className="float-right font-weight-bold">8 KM</span></li>
                                            <li className="list-group-item px-0">Distance from Railway Station <span className="float-right font-weight-bold">1 KM</span></li>
                                            <li className="list-group-item px-0">Distance from Bus Station: <span className="float-right font-weight-bold">1 KM</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            <Footer />   
            </Fragment>
        );
    }
}

export default Contact;