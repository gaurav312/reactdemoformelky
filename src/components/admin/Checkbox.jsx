import React, { Component, Fragment} from "react";

class Checkbox extends Component {
    render() {
        return(
            <Fragment>
                <input 
                    type='checkbox' 
                    name={this.props.name}
                    value={this.props.value}
                    // checked={this.props.checked}
                    // onChange=''
                />
            </Fragment>
        );
    }
}

export default Checkbox;