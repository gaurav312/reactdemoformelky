import React, { Component, Fragment } from 'react';
import './admin.css';
import AdminHeader from './AdminHeader';
import Axios from 'axios';
import { baseApi } from '../../utils';
 
class AddMembers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            first_name  : "Markd",
            last_name   : "zuck",
            email       : "markd12@gmail.com",
            mobile      : "+91 9856323256",
            age         : "25",
            gender      : "",
            dob         : "1997-07-17",
            address     : "12-house, america",

        };

        this.handleChange = this.handleChange.bind(this);
        this.membersFrom = this.membersFrom.bind(this);

    }
          
    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});   
    } 
    
    membersFrom(event) {
        event.preventDefault();
        let memberDetails = {
            first_name      : this.state.first_name,
            last_name       : this.state.last_name,
            email           : this.state.email,
            mobile          : this.state.mobile,
            age             : this.state.age,
            gender          : this.state.gender,
            dob             : this.state.dob,
            address         : this.state.address,
            msgRes          : [],
            messageclasses  : false,
            successMessage  : false
        }
        
        let token = sessionStorage.getItem('jwtToken');
        Axios.post(baseApi + 'member/',memberDetails, { headers: {"Authorization" : `JWT ${token}`} }).then(res =>{
            if(res.status === 201) {
                this.setState({successMessage: true})
                setTimeout(
                    function() {
                        this.setState({successMessage: false});
                    }.bind(this),5000
                );
            }
        }).catch(error => {
            if (error.response) {
                if(error.response.status === 401) {
                    this.props.history.push('/admin-login');
                }else {
                    let statusCode  = error.response.status;
                    let errorMsg    = error.response.data;
                    this.setState({msgRes: {'statusCode':statusCode, 'errorMsg':errorMsg}})
                    this.setState({messageclasses: true})
                    setTimeout(
                        function() {
                            this.setState({messageclasses: false});
                        }.bind(this),5000
                    );
                }
            }
        });
    }
   
    render() {
        return(
            <Fragment>
                <div className="admin_panel">
                    <AdminHeader />
                    <section className="pdng_t_124">
                            
                    </section>
                </div>
            </Fragment>
        );
    }
}

export default AddMembers;