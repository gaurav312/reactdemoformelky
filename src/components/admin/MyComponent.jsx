import React, { Component, Fragment } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { baseApi } from '../../utils';
import Checkbox from './Checkbox';
import MessageModel from '../model/MessageModel'

class MyComponent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isAllSelected: false,
      userDetails: []
    }
    this.handleAllChecked = this.handleAllChecked.bind(this);
  }

  componentDidMount() {
    let token = sessionStorage.getItem('jwtToken');
    axios.get(baseApi + 'member/', { headers: {"Authorization" : `JWT ${token}`} }).then(res => {
        this.setState({userDetails: res.data.results})
    }).catch(error => {
      if (error.response) {
        if(error.response.status === 401) {
          // this.props.history.push('/admin-login');
        }
          // let statusCode  = error.response.status;
          // let errorMsg    = error.response.data.non_field_errors;
          // this.setState({msgRes: {'statusCode':statusCode, 'errorMsg':errorMsg}})
          // this.setState({messageclasses: true})
          // setTimeout(
          //     function() {
          //         this.setState({messageclasses: false});
          //     }.bind(this),5000
          // );
      }
   });
  }
  handleAllChecked = (event) => {
    console.log('-*-*--*-*-*');
    let fruites = this.state.fruites
    fruites.forEach(fruite => fruite.isChecked = event.target.checked) 
    this.setState({fruites: fruites})
  }

  render() {
    let memberDetails   = this.state.userDetails.map((obj)=> {
      obj.checkbox      = <Checkbox   value={obj.id} name={obj.first_name} />;
      obj.action        = <MessageModel id={obj.id} />
      return obj;
    })

    let columns = [
      {
        name: 
          <Checkbox 
            name="select-all" 
            onChange={this.handleAllChecked}  value="checkedall"
          />, 
        selector: 'checkbox', 
        sortable: false,
      },
      {
        name: 'first_name', selector: 'first_name', sortable: true,
      },
      {
        name: 'Last_name', selector: 'last_name', sortable: true, right: true,
      },
      {
        name: 'Email', selector: 'email', sortable: false, right: true,
      },
      {
        name: 'Dob', selector: 'dob', sortable: false, right: true,
      },
      {
        name: 'Gender', selector: 'gender', sortable: false, right: true,
      },
      {
        name: 'Address', selector: 'address', sortable: false, right: true,
      },
      {
        name: 'Age', selector: 'age', sortable: false, right: true,
      },
      {
        name: 'Mobile', selector: 'mobile', sortable: false, right: true,
      },
      {
        name: 'Created', selector: 'created', sortable: false, right: true,
      },
      {
        name: 'Action', selector: 'action', sortable: false, right: true,
      },
    ];

   return(
     <Fragment>
       <DataTable title="Arnold Movies" columns={columns} data={memberDetails} />
     </Fragment>
   ); 
  } 
}

export default MyComponent;