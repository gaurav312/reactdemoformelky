import React, { Component, Fragment } from "react";
import axios from "axios";
import { baseApi } from "../../../utils"
import '../admin.css';

class AdminLogin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            msgRes: [],
            messageclasses: false,
        }
        
        this.loginOpration = this.loginOpration.bind(this);
        this.handleUser = this.handleUser.bind(this); 

    }

    handleUser(event) {
        this.setState({[event.target.name]: event.target.value});          
    };

    loginOpration(event) {
        event.preventDefault();
        let userData = {
            username: this.state.username,
            password: this.state.password
        }

        axios.post(baseApi+`jwt/api-token-auth/`, userData )
        .then(res => {
            if(res) {
                this.props.history.push("/dashboard");
                sessionStorage.setItem('jwtToken', res.data.token);    
            }
        }).catch(error => {
            if (error.response) {
                let statusCode  = error.response.status;
                let errorMsg    = error.response.data;
                this.setState({msgRes: {'statusCode':statusCode, 'errorMsg':errorMsg}})
                this.setState({messageclasses: true})
                setTimeout(
                    function() {
                        this.setState({messageclasses: false});
                    }.bind(this),5000
                );
            }
        });
    }

    render() {
        let message     = this.state.messageclasses;
        if(this.state.msgRes.statusCode === 400){
            let username    = this.state.msgRes.errorMsg.username;
            let password    = this.state.msgRes.errorMsg.password;
            let wrongunm    = this.state.msgRes.errorMsg.non_field_errors; 

            return(
                <Fragment>
                    <section className="margin_top bg_gredient">
                        <div className="col-md-4 main_card">
                            <div className={message ? 'alert alert-danger' : 'd-none'}>
                                {wrongunm}
                                <span>{username}</span>
                                <p className="px-0 py-0">{password}</p>
                            </div> 
                            <div className="card mb-3 card_with_icon2 pt-3">
                                <div className="card-body">
                                    <h4 className="m_t_10 card-title text-center text_primary">Sign In</h4>
                                    <form onSubmit={this.loginOpration} method="POST" autoComplete="off">
                                        <div className="form-group text_primary">
                                            <label htmlFor="exampleInputEmail1">User Name</label>
                                            <input 
                                                type="text" 
                                                name="username"
                                                className="form-control" 
                                                id="exampleInputEmail1" 
                                                aria-describedby="emailHelp" 
                                                placeholder="Enter user name"
                                                value={ this.state.username }
                                                onChange={ this.handleUser } 
                                            />
                                        </div>
                                        <div className="form-group text_primary">
                                            <label htmlFor="exampleInputPassword1">Password</label>
                                            <input 
                                                type="password" 
                                                name="password"
                                                className="form-control" 
                                                id="exampleInputPassword1" 
                                                placeholder="Password"
                                                value={ this.state.password }
                                                onChange={ this.handleUser } 
                                            />
                                        </div>
                                        <button type="submit" className="btn-signin sign-marg-no border-0 w-100 mb-5 btn text-uppercase text-white float-right agent_btn_cancel" >Sign In</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </Fragment>
            );
        }else {
            return(
                <Fragment>
                    <section className="vh_100 bg_gredient ptb_90_80">
                        <div className="col-12 main_card">
                            {/* <div className={message ? 'alert alert-danger' : 'd-none'} >{username}<br/>{password}<br/></div> */}
                            <div className="card box_shw2 border-0 w_400 mx_auto">
                                <div className="card-body">
                                    <h4 className="card-title font-weight-bold text-center text_primary">Sign In</h4>
                                    <form onSubmit={this.loginOpration} method="POST" autoComplete="off">
                                        <div className="form-group text_primary">
                                            <label htmlFor="exampleInputEmail1">User Name</label>
                                            <input 
                                                type="text" 
                                                name="username"
                                                className="form-control" 
                                                id="exampleInputEmail1" 
                                                aria-describedby="emailHelp" 
                                                placeholder="Enter user name"
                                                value={ this.state.username }
                                                onChange={ this.handleUser } 
                                            />
                                        </div>
                                        <div className="form-group text_primary">
                                            <label htmlFor="exampleInputPassword1">Password</label>
                                            <input 
                                                type="password" 
                                                name="password"
                                                className="form-control" 
                                                id="exampleInputPassword1" 
                                                placeholder="Password"
                                                value={ this.state.password }
                                                onChange={ this.handleUser } 
                                            />
                                        </div>
                                        <button type="submit" className="btn bg_primary text-white r_30 p_x_30 p_y_10 mx_auto d-block m_b_20" >Sign In</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </Fragment>
            );
        }
       
    }
}

export default AdminLogin;