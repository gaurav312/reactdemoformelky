import React, { Component, Fragment } from 'react';
import './admin.css';
import AdminHeader from './AdminHeader';
import Axios from 'axios';
import { baseApi } from '../../utils';
 
class AddMembers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            first_name  : "Markd",
            last_name   : "zuck",
            email       : "markd12@gmail.com",
            mobile      : "+91 9856323256",
            age         : "25",
            gender      : "",
            dob         : "1997-07-17",
            address     : "12-house, america",

        };

        this.handleChange = this.handleChange.bind(this);
        this.membersFrom = this.membersFrom.bind(this);

    }
          
    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});   
    } 
    
    membersFrom(event) {
        event.preventDefault();
        let memberDetails = {
            first_name      : this.state.first_name,
            last_name       : this.state.last_name,
            email           : this.state.email,
            mobile          : this.state.mobile,
            age             : this.state.age,
            gender          : this.state.gender,
            dob             : this.state.dob,
            address         : this.state.address,
            msgRes          : [],
            messageclasses  : false,
            successMessage  : false
        }
        
        let token = sessionStorage.getItem('jwtToken');
        Axios.post(baseApi + 'member/',memberDetails, { headers: {"Authorization" : `JWT ${token}`} }).then(res =>{
            if(res.status === 201) {
                this.setState({successMessage: true})
                setTimeout(
                    function() {
                        this.setState({successMessage: false});
                    }.bind(this),5000
                );
            }
        }).catch(error => {
            if (error.response) {
                if(error.response.status === 401) {
                    this.props.history.push('/admin-login');
                }else {
                    let statusCode  = error.response.status;
                    let errorMsg    = error.response.data;
                    this.setState({msgRes: {'statusCode':statusCode, 'errorMsg':errorMsg}})
                    this.setState({messageclasses: true})
                    setTimeout(
                        function() {
                            this.setState({messageclasses: false});
                        }.bind(this),5000
                    );
                }
            }
        });
    }
   
    render() {
        let successMessage  = this.state.successMessage;
        let message         = this.state.messageclasses;
        if(this.state.msgRes){
            let email = this.state.msgRes.errorMsg.email;
            let mobile = this.state.msgRes.errorMsg.mobile;
            let dob = this.state.msgRes.errorMsg.dob;

            return(
                <Fragment>
                    <div className="admin_panel">
                        <AdminHeader />
                        <section className="pdng_t_124">
                            <div className="contact_page_card border-0 card bx_shw2 move_up_100 mx-auto pdng_40 pdng_l_30 pdng_r_30 mrg_b_40 w-100">
                                <div className={message ? 'alert alert-danger' : 'd-none'} >{email}<br/>{mobile}<br/>{dob}</div>
                                <div className={successMessage ? 'alert alert-success' : 'd-none'} >Member has been add successfull.</div>
                                <form className="contact_form" onSubmit={this.membersFrom}> 
                                    <h3 className="contact_heading mrg_b_20 text_primary">Add Member</h3>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="first_name">First Name</label>
                                                <input 
                                                    type="text" 
                                                    name="first_name"
                                                    id="first_name"
                                                    className="form-control"
                                                    placeholder="Enter first name"
                                                    value={ this.state.first_name }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="last_name">Last Name</label>
                                                <input 
                                                    type="text" 
                                                    name="last_name"
                                                    id="last_name" 
                                                    className="form-control" 
                                                    placeholder="Enter last name"
                                                    value={ this.state.last_name }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="email">Email address</label>
                                                <input 
                                                    type="email" 
                                                    name="email"
                                                    className="form-control" 
                                                    id="email"
                                                    placeholder="Enter email"
                                                    value={ this.state.email }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                        <div className="form-group text_primary">
                                                <label htmlFor="phone">Mobile Numbers</label>
                                                <input 
                                                    type="text" 
                                                    name="mobile"
                                                    id="phone" 
                                                    className="form-control"
                                                    placeholder="Enter mobile number"
                                                    value={ this.state.phone }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="dob">DATE OF BIRTH</label>
                                                <input 
                                                    type="text" 
                                                    name="dob"
                                                    id="dob" 
                                                    className="form-control" 
                                                    aria-describedby="emailHelp" 
                                                    placeholder="Enter DATE OF BIRTH"
                                                    value={ this.state.dob }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
    
                                                <label htmlFor="">Gender</label><br/>
    
                                                <input 
                                                    type="radio" 
                                                    name="gender" 
                                                    id="male" 
                                                    value="male"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.male} 
                                                /><label htmlFor="male"> Male</label>
    
                                                <input 
                                                    type="radio"
                                                    name="gender" 
                                                    id="female" 
                                                    value="female"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.female} 
                                                /><label htmlFor="female"> Female</label>
    
                                                <input 
                                                    type="radio" 
                                                    name="gender" 
                                                    id="other" 
                                                    value="other"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.other} 
                                                /><label htmlFor="other"> Other</label>
                        
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                         <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="age">Age</label><br/>
                                                <input 
                                                    type="text"
                                                    name="age"
                                                    id="age" 
                                                    className="form-control"
                                                    value={ this.state.age }
                                                    onChange={ this.handleChange} 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="address">Address</label>
                                                <input 
                                                    type="text" 
                                                    name="address"
                                                    id="address" 
                                                    className="form-control"
                                                    placeholder="Enter address"
                                                    value={ this.state.address }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="btn_wr">
                                        <button type="submit" className="btn text-uppercase text-white float-right agent_btn_cancel">submit</button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </Fragment>
            );
        }else{
            return(
                <Fragment>
                    <div className="admin_panel">
                        <AdminHeader />
                        <section className="pdng_t_124">
                            <div className="contact_page_card border-0 card bx_shw2 move_up_100 mx-auto pdng_40 pdng_l_30 pdng_r_30 mrg_b_40 w-100">
                                <div className={successMessage ? 'alert alert-success' : 'd-none'} >Member has been add successfull.</div>
                                <form className="contact_form" onSubmit={this.membersFrom}> 
                                    <h3 className="contact_heading mrg_b_20 text_primary">Add Member</h3>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="first_name">First Name</label>
                                                <input 
                                                    type="text" 
                                                    name="first_name"
                                                    id="first_name"
                                                    className="form-control"
                                                    placeholder="Enter first name"
                                                    value={ this.state.first_name }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="last_name">Last Name</label>
                                                <input 
                                                    type="text" 
                                                    name="last_name"
                                                    id="last_name" 
                                                    className="form-control" 
                                                    placeholder="Enter last name"
                                                    value={ this.state.last_name }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="email">Email address</label>
                                                <input 
                                                    type="email" 
                                                    name="email"
                                                    className="form-control" 
                                                    id="email"
                                                    placeholder="Enter email"
                                                    value={ this.state.email }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                        <div className="form-group text_primary">
                                                <label htmlFor="phone">Mobile Numbers</label>
                                                <input 
                                                    type="text" 
                                                    name="mobile"
                                                    id="phone" 
                                                    className="form-control"
                                                    placeholder="Enter mobile number"
                                                    value={ this.state.phone }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="dob">DATE OF BIRTH</label>
                                                <input 
                                                    type="text" 
                                                    name="dob"
                                                    id="dob" 
                                                    className="form-control" 
                                                    aria-describedby="emailHelp" 
                                                    placeholder="Enter DATE OF BIRTH"
                                                    value={ this.state.dob }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
    
                                                <label htmlFor="">Gender</label><br/>
    
                                                <input 
                                                    type="radio" 
                                                    name="gender" 
                                                    id="male" 
                                                    value="male"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.male} 
                                                /><label htmlFor="male"> Male</label>
    
                                                <input 
                                                    type="radio"
                                                    name="gender" 
                                                    id="female" 
                                                    value="female"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.female} 
                                                /><label htmlFor="female"> Female</label>
    
                                                <input 
                                                    type="radio" 
                                                    name="gender" 
                                                    id="other" 
                                                    value="other"  
                                                    onChange={this.handleChange} 
                                                    checked={this.state.other} 
                                                /><label htmlFor="other"> Other</label>
                        
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                         <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="age">Age</label><br/>
                                                <input 
                                                    type="text"
                                                    name="age"
                                                    id="age" 
                                                    className="form-control"
                                                    value={ this.state.age }
                                                    onChange={ this.handleChange} 
                                                />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group text_primary">
                                                <label htmlFor="address">Address</label>
                                                <input 
                                                    type="text" 
                                                    name="address"
                                                    id="address" 
                                                    className="form-control"
                                                    placeholder="Enter address"
                                                    value={ this.state.address }
                                                    onChange={ this.handleChange } 
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="btn_wr">
                                        <button type="submit" className="btn text-uppercase text-white float-right agent_btn_cancel">submit</button>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </Fragment>
            );
        }
       
       
    }
}

export default AddMembers;