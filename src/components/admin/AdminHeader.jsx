import React, { Component, Fragment } from "react";
import { Link }  from "react-router-dom";
import './admin.css';
import {img} from '../../static/images/per1.jpg';
import slide1 from '../../static/images/per1.jpg';
class AdminHeader extends Component {
    handleLogout(event) {
        event.preventDefault();
        // localStorage.removeItem('jwtToken');
        sessionStorage.removeItem('jwtToken'); 
        
        window.location.href = ('#/admin-login');
        // window.location.href('/');
        event.preventDefault();
  
    }
    render() {
        return(
            <Fragment>
                <div className="wrapper">
                    {/* <!-- Sidebar --> */}
                    <nav id="sidebar">
                        <div className="sidebar-header text-center bg_primary">
                            <h3 className="p-0 m_b_20"><Link className="text-white f_20_22 font_bold d-block p_y_20 t_d_n" to="/dashboard">Dashboard</Link></h3>
                        </div>
                        <ul className="list-unstyled side_menus">
                            <li><Link to="/bookings" className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-handshake m_r_10 text-primary"></i>Bookings</Link></li>
                            <li><Link to="/coupons" className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-ticket-alt m_r_10 text-primary"></i>Coupons</Link></li>
                            <li><Link to="/rooms" className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-hotel m_r_10 text-primary"></i>Rooms</Link></li>
                            <li><Link to="/payment-history" className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-history m_r_10 text-primary"></i>Payment History</Link></li>
                            <li><Link to="/members" className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-users m_r_10 text-primary"></i>Members</Link></li>
                            {/* <li className="d-block w-100 p_y_10 p_l_30 t_d_n text-white"><i className="fas fa-user-plus svg_icon"></i><Link to="/add-member">Add Members</Link></li> */}
                        </ul>
                    </nav>
                </div>
                <nav className="navbar navbar-expand-lg navbar-light bg-white box_shw">
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <span className="navbar-brand f_16_20">Welcome Admin!</span>
                        <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li className="user-profile header-notification">
                            <div className="dropdown-primary dropdown">
                                <div className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src={slide1} className="img-radius" alt=".." />
                                    <span>Super admin</span>
                                    <i className="feather icon-chevron-down"></i>
                                </div>
                                <ul className="dropdown-menu dropdown_list" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li className="logout" onClick={ this.handleLogout.bind(this)}>
                                    <i className="fas fa-sign-out-alt m_r_10"></i> Logout
                                    </li>
                                </ul>
                            </div>
                            </li>                           
                        </ul>
                    </div>
                </nav>
            </Fragment>
        );
    }
}

export default AdminHeader;