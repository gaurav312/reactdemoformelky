import React, { Component, Fragment } from "react";
import AdminHeader from './AdminHeader';
// import EnhancedTable from './EnhancedTable';
import MyComponent from './MyComponent';
// import CityList from './CityList';
// import axios from "axios";

class MembersList extends Component {


    render() {
        return(
            <Fragment>
                <div className="admin_panel">
                    <AdminHeader />
                    
                    <div className="inner_container">
                        <div className=" border-0 card bx_shw2 mx-auto pdng_40 pdng_l_30 pdng_r_30 mrg_b_40 w-100">
                        
                            <h3 className="contact_heading mrg_b_20 text_primary">Member list</h3>
                            {/* <EnhancedTable /> */}
                            <MyComponent />
                            {/* <CityList /> */}
                            
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
export default MembersList;