import React, { Component,Fragment } from "react";
import { Modal } from 'react-bootstrap';
import './message_model.css';

class MessageModel extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      show: false,
      id: '',
      message: ''
    };

     this.handleClose   = this.handleClose.bind(this);
     this.handleShow    = this.handleShow.bind(this);
     this.handleChange  = this.handleChange.bind(this);
     this.handleSubmit  = this.handleSubmit.bind(this);
  }

  handleClose(event){
    this.setState({ show: false });
  }

  handleShow(event){
    this.setState({ show: true });
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});   
  }

  handleSubmit(event){
    event.preventDefault();
    console.log(this.state);
    // let messageData = {
    //   id      : this.state.id,
    //   message : this.state.message
    // }
  }

  render() {
    return(
      <Fragment>
        <div className="cursor-pointer d-block player_icon position-absolute tran_0_3" onClick={this.handleShow}><i className="fas fa-comments"></i></div>
          <Modal size="lg" show={this.state.show} onHide={this.handleClose} className="modal fade video-modal-lg" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <form onSubmit={this.handleSubmit} method="POST">         
              <Modal.Header closeButton className="border-0 d-block position-relative">
                <Modal.Title className="text-light">Send Message</Modal.Title>
              </Modal.Header>

                <Modal.Body>
                <div className="row">
                  <div className="col">
                    <div className="form-group text_primary">
                      <label htmlFor="">Message</label><br/>
                      <input 
                        type="hidden" 
                        name="id" 
                        value={ this.props.id } 
                        onChange={ this.handleChange }
                      />
                      <textarea name="message"  id={this.props.id} rows="5" cols="90" value={ this.state.message } onChange={ this.handleChange } ></textarea>
                    </div>
                  </div>
                </div>
                <div className="btn_wr_model">
                    <button type="submit" className="btn text-uppercase text-white float-right agent_btn_cancel">Send</button>
                </div>
                </Modal.Body>
              <Modal.Footer className="text-center d-block border-0">
                
              </Modal.Footer>
            </form>
          </Modal> 
      </Fragment>
    );
  }
}
export default MessageModel;