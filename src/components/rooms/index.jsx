import React, { Component, Fragment } from'react';
import Header from '../header';
import Footer from '../footer';

class Room extends Component{

    render(){

        return(
            <Fragment>
            <Header />
            <section className="bg-white p_b_50">
                <div className="container">
                    <div className="col-12">
                        <h1 className="text_primary p_y_50 f_30_34 font-weight-bold text-center">Suites &amp; Rooms</h1>
                            
                            <div className="px-5">
                                <img src="./assets/images/room.jpg" alt="main" className="m_b_30 d-block r_10 mx_auto box_shw2"/>
                                <h2 className="text_primary f_26_28 m_b_20">Tarrif</h2>
                                <p className="para_font">The attentive staff Crown Palace Indore is always available at your service to take care of all your needs. Our extensive menu feature variety of dishes to be served and enjoyed at leisure. Additionally hot and cold snacks and beverages are available throughout the day and evening. Travel Desk for availing booking Taxi for Local Sightseeing or Outstation Travel near Indore &amp; other stations. Feel free to book our Airport, Railway Station and Bus-stand pick-up. Same day laundry services would not let you feel out of home. Doctor on Call is available in case of emergencies.</p>
                            </div>

                            <div className="py-3 px-5">
                                <h3 className="text_primary f_26_28 m_b_20">Something Special - That Little Bit Extra</h3>
                                <p className="para_font">At Crown Palace Indore we pride ourselves in providing that little bit extra, ensuring that our customers come first and our services are flexible and tailor made to suit the needs of our guests. Should it be a late arrival meal, a very early morning breakfast Crown Palace Indore will provide you with whatever you require.</p>
                                <ul className="list-group list-group-flush list-unstyled">
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> 49 well-appointed Guest Rooms and Suites</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Centrally air-conditioned, lavishly furnished well carpeted / wooden floor, attached bath, running hot and cold water, telephones, multi-channel colour TV with remote control, refrigerator, mini-bar and internet connectivity.</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Round the clock well-equipped Room Service</li>
                                </ul> 
                            </div>

                            <div className="py-3 px-5">
                                <h3 className="text_primary f_26_28 m_b_20">Services</h3>
                                <ul className="list-group list-group-flush list-unstyled">
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Writing desk with internet connection</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Refrigerated minibar</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Wardrobe</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Direct dialing local/Intl with voice mail</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> LCD TV</li>
                                    <li className="list-group-item p_5 border-0 bg-transparent"><i className="fas fa-check text_primary m_r_10"></i> Wi-Fi Connectivity</li>
                                </ul> 
                            </div>
                            <div className="py-3 px-5">
                                <a href="./assets/images/tarrif.pdf" className="btn btn-lg bg_primary text-white r_30 f_16_20 p_x_20">Tarrif PDF <i className="fas m_l_5 fa-download"></i></a>
                            </div>
                    </div>
                </div>
            </section>
            <Footer />
            </Fragment>
        );
    }
}

export default Room;