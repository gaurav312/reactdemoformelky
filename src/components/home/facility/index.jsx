import React from 'react';
import {NavLink} from "react-router-dom";

class Facility extends React.Component{

    render() {
        return(
            <div>
                <section className="bg-white ptb_50_40 features_section">
                    <div className="container">
                        <h2 className="text-center m_b_50">Features</h2>
                       <div className="row">
                            <div className="col-12 col-sm-6 col-md-4">
                                <div className="card box_shw3 border-0 m_b_20 hover_shw">
                                    <div className="image_zoom_effect">
                                    <NavLink to="/room">
                                        <img src="./assets/images/suits&room.jpg" className="card-img-top mrg_b_20 hover_scale_11" alt="main" />   
                                    </NavLink>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title text_primary">Suits &amp; Room</h5>
                                        <p className="card-text">At Crown Palace Indore we pride ourselves in providing that little bit extra, ensuring that our customers come first and our services are flexible and tailor made to suit the needs of our guests....</p>
                                        <NavLink to="/room" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white">Find out more <i className="fas fa-arrow-right m_l_10"></i></NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-md-4">
                                <div className="card box_shw3 border-0 m_b_20 hover_shw">
                                    <div className="image_zoom_effect">
                                    <NavLink to="/restraunt">
                                        <img src="./assets/images/restaurants1.jpg" className="card-img-top mrg_b_20 hover_scale_11" alt="main" /> 
                                    </NavLink>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title text_primary">Restaurant &amp; Bar</h5>
                                        <p className="card-text">An awesome open air restaurant that serve you with rich spicy aromas, sits alongside a wide selection of delicious dishes while providing room for fresh breezes and views of the surrounding nature...</p>
                                        <NavLink to="/restraunt" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white">Find out more <i className="fas fa-arrow-right m_l_10"></i></NavLink>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-md-4">
                                <div className="card box_shw3 border-0 m_b_20 hover_shw">
                                    <div className="image_zoom_effect">
                                    <NavLink to="/restraunt">
                                        <img src="./assets/images/restaurants1.jpg" className="card-img-top mrg_b_20 hover_scale_11" alt="main" /> 
                                    </NavLink>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title text_primary">Fast Food Shop</h5>
                                        <p className="card-text">The award winning Fast Food Shop at Hotel Crown Palace are run by Indore's best and most welcoming teams. Come for delicious afternoon teas on Cafe Bakewell...</p>
                                        <NavLink to="/restraunt" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white">Find out more <i className="fas fa-arrow-right m_l_10"></i></NavLink>
                                    </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Facility;