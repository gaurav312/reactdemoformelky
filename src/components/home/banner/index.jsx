import React, { Component, Fragment} from 'react';

class Banner extends Component {

    constructor() {
        super();
        this.state = {
            checkindate: "",
            checkout: ""
        }
    }

    render() {
        return(
            <Fragment>
                <div className="banner-custom relative">
                    <div className="banner-img">
                        <img src="./assets/images/background1.png" alt="baner_image" />
                        <img src="./assets/images/background.png" alt="baner_image" />
                        <img src="./assets/images/home-img-2.png" alt="baner_image" />
                    </div>
                    <div className="address-head text-center">
                        <h1 className="lg font-weight-bold text-white f_40_44 p_y_20 m_t_40">HOTEL CROWN PALACE</h1>
                        <p className="sm text-white f_26_30">2 A, Kanchan Bagh South Tukoganj Near Geeta Bhavan Square,Indore</p>
                    </div>
                    <div className="reservation-form">
                        <form className="booking-form p_30 box_shw2  w_800 mx_auto bg-white r_5" method="get" action="#/booking" autoComplete="off">
                            <h4 className="text-center mb-4 text-uppercase text_primary">Reservation </h4>
                            <div className="form-row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group mb-0">
                                        <label htmlFor="checkindate" className="text_primary">Check In Date 
                                            <span className="text-danger">*</span> 
                                        </label>
                                        <input 
                                            type="text" 
                                            className="form-control rounded-0" 
                                            name="checkin" 
                                            id="checkindate" 
                                            placeholder="dd/mm/yy" 
                                            size="30" 
                                            required="required" 
                                        />
                                        <label htmlFor="checkindate"> 
                                            <span className="date-icon p-1">
                                                <i className="far fa-calendar-alt"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group mb-0">
                                        <label htmlFor="checkoutdate" className=" text_primary">Check out Date
                                        <span className="text-danger">*</span>
                                        </label>
                                        <input 
                                            type="text" 
                                            className="form-control rounded-0" 
                                            name="checkout" 
                                            id="checkoutdate" 
                                            placeholder="dd/mm/yy" 
                                            size="30" 
                                            required="required" 
                                        />
                                        <label htmlFor="checkoutdate"> <span className="date-icon p-1"><i className="far fa-calendar-alt"></i></span>
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group col-12 mb-0 text-center">
                                    <input type="submit" value="Book Now" className="btn btn-primary f_16_18 p_y_10 border-0 r_30  p_x_30 text-white" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </Fragment>
        );
    }

}

export default Banner;