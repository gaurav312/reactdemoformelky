import React, { Component, Fragment} from 'react';
import Banner from './banner/index.jsx';
import Tabs from './tabs/index.jsx';
import Facility from './facility/index.jsx';
import Header from '../header'
import Footer from '../footer'

class Home extends Component {
    render() {
        return(
            <Fragment>
                <Header />
                <Banner />
                <Tabs />
                <Facility />
                <Footer />
            </Fragment>
        );
    }
}

export default Home;