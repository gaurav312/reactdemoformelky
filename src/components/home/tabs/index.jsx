import React, { Component, Fragment} from 'react';

class Tabs extends Component{

    constructor(props){
        super(props);

        this.state = {
            ref: "#"
        }
    }

    render() {
        return (
            <Fragment>
            <section className="tabs bg-light ptb_50_40">
            <div className="container">
            <h2 className="text-center m_b_50">About Crownpalace</h2>
                <div className="row">
                    <div className="col-12">
                        <nav>
                            <div className="nav nav-tabs nav-fill border-0" id="nav-tab" role="tablist">
                                <a className="nav-item nav-link border-0 p_y_20 active" id="nav-hotel-tab" data-toggle="tab" href="#nav-hotel" role="tab" aria-controls="nav-hotel" aria-selected="true">Hotel Info</a>
                                <a className="nav-item nav-link border-0 p_y_20 text-secondary" id="nav-facility-tab" data-toggle="tab" href="#nav-facility" role="tab" aria-controls="nav-facility" aria-selected="false">Facilities</a>
                                <a className="nav-item nav-link border-0 p_y_20 text-secondary" id="nav-gallery-tab" data-toggle="tab" href="#nav-gallery" role="tab" aria-controls="nav-gallery" aria-selected="false">Gallery</a>
                            </div>
                        </nav>
                        <div className="tab-content m_b_20 box_shw3 py-3 p_x_20 bg-white" id="nav-tabContent">
                            <div className="tab-pane fade show active pt-3" id="nav-hotel" role="tabpanel" aria-labelledby="nav-hotel-tab">
                                <p className="pt-3">We take immense pleasure to introduce ourselves as a 3-star facility hotel. We are well recognized by the elite of the city and a large number of companies of repute. We have 49 well-appointed Guest Rooms and Suites.We also have Banquet Halls, Conference Halls, Business center; all keeping in mind the need of todays business travelers. Also uniquely designed multi-cuisine Restaurants, In - House Bakery, Fast Food, Cocktail lounge and Bar-Be-Que to comfort your guests the way they want. Above all, a team of professionally trained staff rendering your guests with quality oriented personalized services.</p>
                                <p><span className="font-weight-bold">Location : </span>The beautifully built hotel, located in prime business and shopping area, is a comfortable 20-minutes drive from the airport and a 05-minutes drive from the railway station. Hotel is located in the center of the city on Agra-Mumbai Highway, which makes access to the hotel very convenient.</p>
                            </div>
                            <div className="tab-pane fade pt-3" id="nav-facility" role="tabpanel" aria-labelledby="nav-facility-tab">
                                <div className="row pt-3">
                                    <div className="col col_padding">
                                        <ul className="list-group d-inline-block">
                                        <li className="list-group-item mr-2 d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span> Banquet</li>
                                        <li className="list-group-item mr-2 d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span>Meeting Facilities</li>
                                        <li className="list-group-item mr-2 d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span>Valet Parking</li>
                                        </ul>
                                    </div>
                                    <div className="col col_padding">
                                        <ul className="list-group d-inline-block">
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span>Concierge</li>
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span>Parking</li>
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2"><i className="fas fa-check-circle"></i></span>Laundry</li>
                                        </ul>
                                    </div>
                                    <div className="col col_padding">
                                        <ul className="list-group d-inline-block">
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2 d-inline-block"><i className="fas fa-check-circle"></i></span>Conference Facilities</li>
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2 d-inline-block"><i className="fas fa-check-circle"></i></span>Restaurant</li>
                                        <li className="list-group-item d-inline-block border-0 w-100"><span className="mr-2 d-inline-block"><i className="fas fa-check-circle"></i></span>Luggage Storage</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade pt-3" id="nav-gallery" role="tabpanel" aria-labelledby="nav-gallery-tab">
                                <div className="row">
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g1.jpg" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g2.jpg" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g3.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="w-100"></div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g4.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g5.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g6.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="w-100"></div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g7.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g8.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                    <div className="col">
                                        <a href="{ref}" className="d-block mb-2"><img src="./assets/images/g9.gif" className="gallery_image img-fluid w-100 r_10 m_b_20" alt="images" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </Fragment>
        );
    }
}

export default Tabs;