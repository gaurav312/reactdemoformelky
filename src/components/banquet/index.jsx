import React, { Component, Fragment} from'react';
import Header from '../header';
import Footer from '../footer';

class Banquet extends Component{

    render() {
        return(
            <Fragment>
            <Header />
            <section className="bg-white p_b_50">
                <div className="container">
                    <div className="col-12">
                        <h1 className="text_primary p_y_50 f_30_34 font-weight-bold text-center">Meetings &amp; Banquets</h1>
                        <div className="text-center">
                            <img src="./assets/images/room.jpg" alt="main" className="m_b_30 d-block r_10 mx_auto box_shw2"/>
                        </div>
                            <div className="accordion py-3 px-5" id="accordionExample"/>
                                <div className="card border-0 box_shw2">
                                    <div id="headingOne">
                                        <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Zodiac Room<span className="float-right"><i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                                    </div>
                                    <div className="accordion py-3 px-5" id="accordionExample"/>
                                        <div className="card">
                                        <div id="headingOne">
                                            <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Zodiac Room
                                                <span className="float-right">
                                                    <svg className="svg-inline--fa fa-plus-circle fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                                    <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path>
                                                    </svg>
                                                    <i className="fas fa-plus-circle"></i> 
                                                </span>
                                            </button>
                                        </div>
                                        <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div className="card-body">
                                                <h2 className="contact_heading">Zodiac Room</h2>
                                                <div className="row">
                                                    <div className="col-8 pt-3">
                                                    <h4 className="card-subtitle mb-2">You take care of the business, we'll take care of the facilities...</h4>
                                                    <p className="card-text para_font">Our professionals will guide you through the planning process, leaving no stone unturned. Right from the booking, business facilities, snacks to stationary and flipcharts we look after the minutest of details to make your event and you a 'success'.</p>
                                                    <ul className="list-group d-inline-block">
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            High-speed internet access
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            LCD Projection
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            Whiteboard & pencilss
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            flipchart
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            Writing paper and pencils
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            Latest audio visual technology
                                                        </li>
                                                        <li className="para_font list-group-item d-inline-block border-0 w-100 py-1">
                                                            <span className="mr-2">
                                                                <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                                <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                                </svg>
                                                                <i className="&#8594 "></i> 
                                                            </span>
                                                            Newspapers & magazines
                                                        </li>
                                                    </ul>
                                                    </div>
                                                    <div className="col-4 pt-3 pb-4">
                                                    <img src="./assets/images/b1.jpg" alt= "main"className="gallery_image img-fluid"/>
                                                    </div>
                                                    <div className="px-3 pt-2">
                                                    <p className="card-text para_font">We do everything possible to help make your conference a success at your requirement.</p>
                                                    <p className="card-text para_font">Spacious, centrally-airconditioned hall Overhead and slide projectors, CD player &amp; TV, PA system with standing and collar mikes, secretarial services, etc.</p>
                                                    <p className="card-text para_font font-weight-bold">Conference room equipped with all modern amenities, suitable for organizing conference / seminars.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="card">
                                    <div id="headingTwo">
                                        <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Crystal Room<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                                    </div>

                                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div className="card-body">
                                            <h2 className="contact_heading">Crystal Room</h2>
                                            <div className="row">
                                                <div className="col-8 pt-3">
                                                    <h4 className="card-subtitle mb-2">There are seminar halls as well which have been well designed and placed meticulously by the trained staff of the hotel.</h4>

                                                    <p className="card-text para_font">For its esteemed business visitors, the hotel has equipped these halls with all modern amenities required to hold a conference, meeting, seminar or presentation.</p>

                                                    <ul className="list-group d-inline-block">
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg><i className="&#8594 "></i> </span> High-speed internet access</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg>  <i className="&#8594 "></i> </span> LCD Projection</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg> <i className="&#8594 "></i> </span>Whiteboard & pencilss</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg><i className="&#8594 "></i> </span>flipchart</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg> <i className="&#8594 "></i> </span>Writing paper and pencils</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg> <i className="&#8594 "></i> </span> Latest audio visual technology</li>
                                                    <li className="para_font list-group-item d-inline-block border-0 w-100 py-1"><span className="mr-2"><svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path></svg> <i className="&#8594 "></i> </span>Newspapers & magazines</li>
                                                </ul>
                                                </div>
                                                <div className="col-4 pt-3 pb-4">
                                                    <img src="./assets/images/b2.jpg" alt="main" className="gallery_image img-fluid"/>
                                                </div>
                                                <div className="px-3 pt-2">
                                                    <p className="card-text para_font font-weight-bold">Lavishly designed conference hall furnished with modern amenities, suitable for organising training programmes and conferences.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="card">
                                    <div id="headingThree">
                                        <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Imperial Hall<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                                    </div>

                                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                        <div className="card-body">
                                            <h2 className="contact_heading">Imperial Hall</h2>
                                            <div className="row text-center">
                                                <div className="col-6 col-md-4 pt-3 pb-4">
                                                    <img src="./assets/images/b3.jpg" alt="main" className="gallery_image img-fluid res_gallery pr-4"/>
                                                </div>
                                                <div className="col-6 col-md-4 pt-3 pb-4">
                                                    <img src="./assets/images/b4.jpg" alt="main" className="gallery_image img-fluid res_gallery"/>
                                                </div>
                                                <p className="card-text para_font">An elaborate banquet hall, equipped with all sort of banquet amenities, suitable for organising seminars, family functions for up to 200 people.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="card">
                                    <div id="headingFour">
                                        <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Regency Hall<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                                    </div>

                                    <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                        <div className="card-body">
                                            <h2 className="contact_heading">Regency Hall</h2>
                                            <div className="row text-center">
                                                <div className="col-6 col-md-4 pt-3 pb-4">
                                                    <img src="./assets/images/b5.jpg" alt="main" className="gallery_image img-fluid res_gallery pr-4"/>
                                                </div>
                                                <div className="col-6 col-md-4 pt-3 pb-4">
                                                    <img src="./assets/images/b6.jpg" alt="main"className="gallery_image img-fluid res_gallery"/>
                                                </div>
                                                <p className="card-text para_font">More elaborate banquet hall equipped with stage and dance floor, suitable for organising seminars, family functions etc. for up to 300 people.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="card">
                                    <div id="headingFive">
                                        <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">Fact Sheet<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                                    </div>

                                    <div id="collapseFive" className="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                        <div className="card-body">
                                            <h2 className="contact_heading">Fact Sheet</h2>
                                            <h4 className="contact_heading py-3">Capacity</h4>
                                            <div className="row">
                                                <table className="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Venues</th>
                                                            <th scope="col">Size</th>
                                                            <th scope="col">Sitting</th>
                                                            <th scope="col">Reception</th>
                                                            <th scope="col">Minimum Pax</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">BOARD ROOM I</th>
                                                            <td></td>
                                                            <td>08</td>
                                                            <td>-</td>
                                                            <td>06</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">BOARD ROOM II</th>
                                                            <td></td>
                                                            <td>14</td>
                                                            <td>-</td>
                                                            <td>08</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">ZODIAC ROOM</th>
                                                            <td>33'×17'</td>
                                                            <td>30</td>
                                                            <td>-</td>
                                                            <td>20</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">CRYSTAL ROOM</th>
                                                            <td>37'×30'</td>
                                                            <td>40</td>
                                                            <td>-</td>
                                                            <td>25</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">IMPERIAL HALL</th>
                                                            <td>58'×40'×10'</td>
                                                            <td>150</td>
                                                            <td>200</td>
                                                            <td>75</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">REGENCY HALL</th>
                                                            <td>80'×37'×10'</td>
                                                            <td>200</td>
                                                            <td>250</td>
                                                            <td>100</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                </div>
                                                <h4 className="contact_heading py-3">Rentals</h4>
                                                <div>
                                                    <table className="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Venues</th>
                                                            <th scope="col">8 Hours</th>
                                                            <th scope="col">4 Hours</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">BOARD ROOM I</th>
                                                            <td>4,000/-</td>
                                                            <td>2,000/-</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">BOARD ROOM II</th>
                                                            <td>5,000/-</td>
                                                            <td>3,000/-</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">ZODIAC ROOM</th>
                                                            <td>8,500/-</td>
                                                            <td>6,000/-</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">CRYSTAL ROOM</th>
                                                            <td>12,000/-</td>
                                                            <td>9,000/-</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">IMPERIAL HALL</th>
                                                            <td>25,000/-</td>
                                                            <td>17,000/-</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">REGENCY HALL</th>
                                                            <td>30,000/-</td>
                                                            <td>20,000/-</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                </div>
                                                <div className="py-3">
                                                    <a href="./assets/images/banquet.pdf" className="btn agent_btn_cancel" role="button" aria-pressed="true">
                                                    Download Banquet Fact Sheet
                                                    <span className="ml-2">
                                                        <svg className="svg-inline--fa fa-arrow-up fa-w-14 arrow" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                                            <path fill="currentColor" d="M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z"></path>
                                                        </svg>
                                                        <i className="fas fa-arrow-up arrow"></i> 
                                                    </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                            </div>
                        </div>
                   
                
            </section>
            <Footer />
            </Fragment>
        );
    }
}

export default Banquet;