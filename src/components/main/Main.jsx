import React, { Component, Fragment} from'react';
import RoomBooking from '../booking/RoomBooking';  
import UserDetails from '../user-details/userDetails';
import qs from "query-string";

class Main extends Component {
  
  constructor() {
    let url = qs.parse(window.location.search);
    super()
    this.state = {
      step: 1,
      
      // step 1
      checkin: url.checkin,
      checkout: url.checkout,
      rooms: [
          { room: "1", adult: "1", kid: "5" }
      ],
  
      //step 2
      jobTitle: "",
      jobCompany: "",
      jobLocation: ""
    }

  }
  
  
  nextStep = () => {
    const {step} = this.state;

    this.setState({
      step: step +1
    })
  }

  prevStep = () => {
    const {step} = this.state;

    this.setState({
      step: step -1
    })
  }

    handleShareholderNameChange = idx => evt => {
        const newrooms = this.state.rooms.map((shareholder, sidx) => {
        if (idx !== sidx) return shareholder;
            let name = evt.target.name
            return { ...shareholder, [name]: evt.target.value };
        });
        this.setState({ rooms: newrooms });
    };

  handleRemoveShareholder = idx => () => {
      console.log(idx)
      this.setState({
          rooms: this.state.rooms.filter((s, sidx) => idx !== sidx)
      });
      //TODO Reindexing filter  
  };

  handleChange = input => e => {
    this.setState({[input]: e.target.value})
  }

  showStep = () => {
    const {step} = this.state;
    if(step === 1)
    return(
        <RoomBooking
          handleRemoveShareholder = {this.props}
          handleChange = {this.handleChange}
          nextStep = {this.nextStep}
        />
    );
    if(step === 2)
        return(
            <UserDetails 
                handleChange = { this.handleChange }
                nextStep = { this.nextStep }
                prevStep =  {this.prevStep }
            />
        );
  }

  render() {
      return(
          <Fragment>
              {this.showStep()}
          </Fragment>
      );
  }
}

export default Main;