import React from 'react';

class Footer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            reservation: "http://reservation.com",
            webllisto: "http://webllisto.com",
        }
    }

    render() {
        return (
            <footer className="footer_design1 bottom_header f_14_16">
                <div className="container p-3"> 
                    <div className="row"> 
                        <div className="col-12 col-md-4">
                            <a className="text_primary" href={this.state.reservation}>Reservation and Cancellation Policy </a>
                        </div>
                        <div className="col-12 col-md-4 text-center">  
                            © 2019 All Rights Reserved.
                        </div>  
                        <div className="col-12 col-md-4 text-right">
                            Designed By: <a className="text_primary" href="#" target="_blank" title="CROWN" rel="noopener noreferrer">CROWN </a>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;