import React, { Component, Fragment } from'react';
import Header from '../header';
import Footer from '../footer';

class Restraunt extends Component{
    render(){
        return(
         <Fragment>
         <Header />
         <section className="bg-white p_b_50">
            <div className="container">
               <div className="col-12">
                  <h1 className="text_primary p_y_50 f_30_34 font-weight-bold text-center">Restaurants &amp; Bars</h1>
                  <div className="text-center">
                     <img src="./assets/images/restaurants1.jpg" Alt="main" className="m_b_30 d-block r_10 mx_auto box_shw2"/>
                  </div>
                  <div className="accordion py-3 px-5" id="accordionExample">
                     <div className="card border-0 box_shw2">
                        <div id="headingOne">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Golden Treat<span className="float-right">   <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                        </div>
                        <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Golden Treat</h2>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/gt1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/gt2.jpg" Alt="main" className="img-fluid  r_10"/>
                                 </div>
                              </div>
                              <p className="card-text para_font">An exclusive family Restaurant, artistically decorated in the hues of gold, famous for its ambience and multi cuisine.</p>
                           </div>
                        </div>
                     </div>
                     <div className="card">
                        <div id="headingTwo">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Sizzling Treat<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i></span></button>
                        </div>
                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Sizzling Treat</h2>
                              <h4 className="contact_heading py-2">Sizzlers and Bar-Be-Que</h4>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/st1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/st2.jpg"Alt="main" className="img-fluid  r_10"/>
                                 </div>
                              </div>
                              <p className="card-text para_font">An awesome open air restaurant that serve you with rich spicy aromas, sits alongside a wide selection of delicious dishes while providing room for fresh breezes and views of the surrounding nature.An exclusive family Restaurant, artistically decorated in the hues of gold, famous for its ambience and multi cuisine.</p>
                           </div>
                        </div>
                     </div>
                     <div className="card">
                        <div id="headingThree">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Terrace Treat<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i></span></button>
                        </div>
                        <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Terrace Treat</h2>
                              <p className="card-text para_font">The charming Rooftop Restaurant located on the top floor boasts spectacular views of the skyline. The restaurant is open daily for dinner. Share the good times and come dine with us for fun, food, and spirits. And rememberances.</p>
                              <h4 className="contact_heading py-2">Roof Top Bar-Be-Que</h4>
                              <p className="card-text para_font">An exotic open air Bar-Be-Que at the terrace offering you a choice of mouth watering Veg and Non Veg delicacies straight out of Angithi Handi and Tandoor.</p>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/tt1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/tt2.jpg" Alt="main"className="img-fluid  r_10"/>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="card">
                        <div id="headingFour">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">Lancer Cocktail Lounge<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i></span></button>
                        </div>
                        <div id="collapseFour" className="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Lancer Cocktail Lounge</h2>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/lcl1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/lcl2.jpg" Alt="main" className="img-fluid  r_10"/>
                                 </div>
                              </div>
                              <p className="card-text para_font">Beautifully fashioned, highly coveted and full fledged cocktail lounge. Choice of brands, cocktails and mocktails presented and served in a gracious manner.<br/>
                                 With a truly sophisticated atmosphere, lancer Cocktail Lounge is an ideal place for pre-dinner or after work cocktails, with a selection of small plates inspired by menu. A warm, friendly service, makes this a popular spot for an informal gathering, business meeting, or sharing a few packs with friends.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div className="card">
                        <div id="headingFive">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">Cafe Bake Well<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i></span></button>
                        </div>
                        <div id="collapseFive" className="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Cafe Bake Well</h2>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/cbw1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/cbw2.jpg" Alt="main" className="img-fluid  r_10"/>
                                 </div>
                              </div>
                              <p className="card-text para_font">Cafe that serves you with variety of delicious pizzas, burgers, sandwiches, milk shakes, hot dogs, ice-creams, smoothies, coffee and south Indian cuisines etc.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div className="card">
                        <div id="headingSix">
                           <button className="card-header btn btn-lg-link btn-block bg_primary font-weight-bold text-white text-left" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">Bake Well Bakery<span className="float-right"> <i className="fas fa-plus"></i><i className="fas fa-minus"></i> </span></button>
                        </div>
                        <div id="collapseSix" className="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                           <div className="card-body">
                              <h2 className="contact_heading">Bake Well Bakery</h2>
                              <div className="row">
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/bake1.jpg" Alt="main" className="img-fluid  m_r_10 r_10"/>
                                 </div>
                                 <div className="col-6 col-md-4 pt-3 pb-4">
                                    <img src="./assets/images/bake2.jpg" Alt="main" className="img-fluid  r_10"/>
                                 </div>
                              </div>
                              <p className="card-text para_font">Take away bakery serves you with delighting fresh baked cakes, cookies, pastries, breads and delicious tasty goodies. A lot more than what one desires from an oven, truly with tastes that mesmerize!
                              </p>
                           </div>
                        </div>
                     </div>
                     <div className="py-4">
                        <a href="./assets/images/restaurant.pdf" className="btn btn-lg bg_primary text-white r_30 f_16_20 p_x_20" role="button" aria-pressed="true">Take Away Menu<span className="ml-2"><i className="fas m_l_5 fa-download"></i></span></a>
                     </div>
                  </div>
               </div>
            </div>
         </section> 
         <Footer />
         </Fragment>
        );
    }
}

export default Restraunt