import axios from "axios";

const baseApi = 'https://469bfaec.ngrok.io/api/';

export const LoginOperations = data =>  {

  return () => {  
    return axios.post(baseApi+"jwt/api-token-auth/") 
      .then(response => {
        console.log(response)
        return response;
      })
      .catch(error => {
        return false;
      });
  };

}
