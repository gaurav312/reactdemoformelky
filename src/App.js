import React from 'react';
import {Route, Switch, HashRouter} from "react-router-dom";
import Home from'./components/home/index.jsx';
import About from'./components/about/index.jsx';
import Room from'./components/rooms/index.jsx';
import Restaurant from'./components/restaurant/index.jsx';
import Contact from'./components/contact/index.jsx';
import Banquet from'./components/banquet/index.jsx';
import Error from'./components/Error.js';
import Booking from'./components/booking/index.jsx';
import AdminLogin from './components/admin/admin-login/AdminLogin';
import Dashboard from './components/admin/Dashboard';
import AddMembers from './components/admin/AddMembers';
import MembersList from './components/admin/MembersList';

class App extends React.Component{
  render(){
    return(
      <React.Fragment>
        <HashRouter>
            <Switch>
                <Route path="/" component= {Home} exact/>
                <Route path="/about" component={About}/>
                <Route path="/contact" component={Contact}/>
                <Route path="/room" component={Room}/>
                <Route path="/booking" component={Booking}/>
                <Route path="/restaurant" component={Restaurant}/>
                <Route path="/banquet" component={Banquet}/>
                <Route path="/error" component={Error}/>
                <Route path="/admin-login" component={AdminLogin} />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/payment-history" component={Dashboard} />
                <Route path="/coupons" component={Dashboard} />
                <Route path="/bookings" component={Dashboard} />
                <Route path="/rooms" component={Dashboard} />
                <Route path="/add-member" component={AddMembers} />
                <Route path="/members" component={MembersList} />
                
             </Switch>
        </HashRouter>
      </React.Fragment>
      );
    }
}

export default App;
