jQuery('document').ready(function($){
   
//    Date Picker
    jQuery('.datePicker').datepicker({
        dateFormat: "dd-M-yy",
        minDate: 0
    });
    
    $("#checkindate").datepicker({
        minDate: 0,
        maxDate: '+1Y+6M',
        onSelect: function (dateStr) {
            var min = $(this).datepicker('getDate'); // Get selected date
            $("#checkoutdate").datepicker('option', 'minDate', min || '0'); // Set other min, default to today
        }
    });

    $("#checkoutdate").datepicker({
        minDate: '0',
        maxDate: '+1Y+6M',
        onSelect: function (dateStr) {
            var max = $(this).datepicker('getDate'); // Get selected date
            $('#datepicker').datepicker('option', 'maxDate', max || '+1Y+6M'); // Set other max, default to +18 months
            var start = $("#checkindate").datepicker("getDate");
            var end = $("#checkoutdate").datepicker("getDate");
            var days = (end - start) / (1000 * 60 * 60 * 24);
            $("#numberofrooms").val(days);
        }
    });
    
//    Search Trigger
    jQuery('.search_trigger').click(function(){
        jQuery('.search_form_design').slideToggle('slow');
    });
    
//    Slick Slider
    $('.banner-img').slick({
        autoplay:true,
        autocontrol:true,
        dots: false,
        infinite: true,
        speed: 10,
        fade:true,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: false,
        nextArrow: false
    });
    
}); 